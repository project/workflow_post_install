DESCRIPTION
=========== 

Workflow Post Install is a utility module containing functions you may want to
execute on existing content after you have installed the Workflow module. This
includes cleanup after deleting old workflows (if needed), and assignment of
workflow states to existing content.

This module was developed in response to various forum discussions, for instance
#183133: Set workflow state on existing nodes to <created> when defining new
workflow..

INSTALLATION
============

1. Place the workflow_post_install folder in "sites/all/modules".
2. Enable the module under Administer >> Site building >> Modules.

NOTE: This module does not differentiate between different sub-sites when
performing operations in a multi-site environment. The only benefit to
installing the module on a single sub-site is to not expose the functionality to
certain front ends (sites).

USAGE
=====

This utility has two functions: to change the state of nodes that are
referencing old, deleted workflows to No state; and to put content that is in No
state (and of a type that participates in workflows, as set on the
/admin/build/workflow page), into a valid workflow state, thus making it
accessible and usable in workflows.

This refers in particular to content that existed before you installed the
Workflow module. Therefore you typically use this module only once, namely
shortly after enabling the Workflow module (after that you may disable or
uninstall it).

There's currently no menu created to invoke this functionality (keep watching
this space - this is under development). You execute it by entering URLs into
the browser. You can also utilize the core functions in your own module, and
there is also support for Drush.

NOTE: The logged-in user must have the "administer workflow" permission (or be
an administrator) to be authorized to use the URLs.  The user must also have
the proper permissions to move nodes form the "(creation)" state to the new
state.  For the Code version, it is up to the calling module to verify
permissions for the current user before using these functions.

WARNING: The functions provided by this module affect the database directly, and
cannot be undone without a database restore. You should backup your database
before using the functions provided by this module.

Cleaning Existing Content
-------------------------

This performs an extensive cleanup of the Workflow tables.  It removes all
orphaned items in all tables, as well as removing all workflow state items
with a status of 0 (deleted).  You can then use the assignment function to
assign a new workflow state to previously orphaned nodes.

URL
---

/content/clean_workflow_state

Code
----

workflow_clean_state();

Drush
-----

workflow-post-install-clean
-OR-
wpi-cl


Assigning Workflow States
-------------------------

This function assigns a workflow state to existing, unassigned content. This
only affects node types that are allowed into the given workflow. If your nodes
are not making it into the new state, verify that your node types are assigned
to the workflow properly on the /admin/build/workflow page, and that you have
permissions to transition a node from (creation) state to the new state.

We call all hooks, transition events, and cleanup just like the Workflow
module's workflow_execute_transition function.

URL
---
/content/set_workflow_state[/[un]published]/<state>

[un]published is the current publication state of the nodes to be affected. This
parameter is optional. If this parameter is not given, the assignment affects
all nodes regardless of publication state.

<state> is the name of a workflow state you want to assign content to. If the
given state is not part of a workflow that is valid for the content type of the
node, nothing will happen.

Code
----

workflow_create_state_for_stateless_nodes($state_name, $published);
$state_name is the name of a workflow state you want to assign content to. If
the given state is not part of a workflow that is valid for the content type of
the node, nothing will happen.

$published is the current publication state of the nodes to be affected. This
parameter is optional. If this parameter is not given or is NULL, the assignment
affects all nodes regardless of publication state. Valid options are NULL
(affect all nodes), 1 (affect published nodes), or 0 (affect unpublished nodes).

The function returns the number of nodes affected. Return of -1 means an invalid state
was specified.

Drush
-----

workflow-post-install-create-state <state> [published]
-OR-
wpi-cs

<state> is the name of a workflow state you want to assign content to. If the
given state is not part of a workflow that is valid for the content type of the
node, nothing will happen.

[published] is the current publication state of the nodes to be affected. This
parameter is optional. If this parameter is not given, the assignment affects
all nodes regardless of publication state. Valid options are 1 (affect published
nodes), or 0 (affect unpublished nodes).

AUTHOR
======
Rik de Boer, Melbourne, Australia
Significant changes by Dennis Wallace, DW Brand
