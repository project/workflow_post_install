<?php

/**
 * Drush integration addon for workflow_post_install
 */

/**
 * Implementation of hook_drush_help()
 */
function workflow_post_install_drush_help($command) {
  switch($command) {
    case 'drush:workflow-post-install-create-state':
    case 'drush:wpi-cs':
      return dt('Makes existing nodes without a current workflow state have a given new workflow state.');
    case 'drush:workflow-post-install-clean':
    case 'drush:wpi-cl':
      return dt('Removes the workflow state of existing nodes whose workflow state is either inactive or whose workflow has been deleted.');
  }
}

/**
 * Implementation of hook_drush_command()
 */
function workflow_post_install_drush_command() {
  $items = array();
  
  $items['workflow-post-install-create-state'] = array(
    'description' => dt('Makes existing nodes without a current workflow state have a given new workflow state.'),
    'arguments' => array(
      'new_state' => dt('New workflow state name'),
      'published' => dt('Optional. Node state to filter on.  0=update unpublished nodes, 1=update published nodes, -1=update all nodes'),
    ),
    'required-arguments' => 1,
    'aliases' => array('wpi-cs'),
  );
  
  $items['workflow-post-install-clean'] = array(
    'description' => dt('Removes the workflow state of existing nodes whose workflow state is either inactive or whose workflow has been deleted.'),
    'arguments' => array(
    ),
    'aliases' => array('wpi-cl'),
  );
  
  return $items;
}

/**
 * Implementation of callback for workflow-post-install-create-state command
 */
function drush_workflow_post_install_create_state($new_state, $published=-1) {
  $workflow = NULL;

  $pub = false;
  if ($published == -1) {
    $pub = NULL;
  }
  elseif ($published == 1) {
    $pub = true;
  }
  
  drush_log(dt('Executing push of stateless nodes to "@state" state.', array('!count' => $r, '@state' => $new_state)), 'status');
  $r = workflow_create_state_for_stateless_nodes($new_state, $pub, $workflow);

  if ($r > 0) {
    drush_log(dt('!count node(s) were put in the "@state" state.', array('!count' => $r, '@state' => $new_state)), 'status');
    drush_log(dt('The content access permissions need to be rebuilt.'), 'warning');
  }
  elseif ($r == 0) {
    drush_log(dt('No changes were made.'), 'warning');
  }
  elseif ($r == -1) {
    drush_log(dt('Invalid workflow state: "@state"', array('@state' => $new_state)), 'error');
  }
  else {
    drush_log(dt('Unknown error.', array()), 'error');
  }
}

/**
 * Implementation of callback for workflow-post-install-create-state command
 */
function drush_workflow_post_install_clean() {
  drush_log(dt('Executing workflow status cleanup for existing nodes.', array()), 'status');
  $r = workflow_clean_state();
  if ($r > 0) {
    drush_log(t('!count node(s) were cleaned.', array('!count' => $r)), 'status');
    drush_log(dt('The content access permissions need to be rebuilt.'), 'warning');
  }
  elseif ($r == 0) {
    drush_log(dt('No changes were made.'), 'warning');
  }
  else {
    drush_log(t('Error cleaning workflow node states.', array()), 'error');
  }
}
